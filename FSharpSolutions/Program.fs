﻿// Learn more about F# at http://fsharp.org

module ProblemSolver
open System

[<EntryPoint>]
let main argv =

    let problemNumber = argv.[0] |> int

    let runSolution (problemNumber : int) = 
        match problemNumber with
        | 5 -> Problem5.solve
        | _ -> 0

    let solution = runSolution problemNumber

    printfn "Solution: %i" solution

    0