module Problem5
    
    let solve =

        let rec checkSolution iterator value =

            if iterator = 21 then value
            else 
                if value % iterator = 0 then checkSolution (iterator + 1) value
                else checkSolution 1 (value + 2520)

        checkSolution 1 2520
            
