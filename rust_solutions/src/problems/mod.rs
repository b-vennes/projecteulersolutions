pub mod problem_6;

pub trait IsProblem{
    fn solve(&self) -> i64;
}