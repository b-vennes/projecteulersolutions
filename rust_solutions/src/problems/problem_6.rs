use crate::problems::IsProblem;

pub struct Problem6 {}

impl IsProblem for Problem6 {
    fn solve(&self) -> i64 {
        let nums_1 = 1..101;
        let nums_2 = 1..101;

        let nums_sum: i64 = nums_1.sum();
        let nums_sum_square: i64 = nums_sum.pow(2);

        let nums_square_sum: i64 = nums_2.map(|x: i64| x.pow(2)).sum();

        nums_sum_square - nums_square_sum
    }
}