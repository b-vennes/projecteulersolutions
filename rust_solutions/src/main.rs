mod problems;

use crate::problems::IsProblem;
use crate::problems::problem_6::Problem6;

fn main() {
    let problem_number = 6;

    let problem = match problem_number {
        6 => Some(Problem6 {}),
        _ => None,
    };

    println!("{}", problem.unwrap().solve());
}
