let splitString (character: char) (s: string) = s.Split [|character|]

let trimStart (s: string) = s.TrimStart ' '

let linesToGrid lines =
    lines
    |> Seq.fold (fun state line -> state + " " + line) ""
    |> trimStart
    |> splitString ' '
    |> Seq.toList
    |> List.map int64

let values input parser = 
    System.IO.File.ReadLines(input)
    |> parser

let rec product (l: int64 list) : int64 =
    if l.IsEmpty then 1L
    else l.Head * (product l.Tail)

let paddingRight (grid: int64 list) width requiredPadding = (grid.Length - 1) % width + 1 >= requiredPadding

let paddingLeft (grid: int64 list) width requiredPadding = width - ((grid.Length - 1) % width + 1) + 1 >= requiredPadding

let paddingDown (grid: int64 list) width requiredPadding = (grid.Length - 1) / width + 1 >= requiredPadding

let calculateDown (grid: int64 list) width adjacentLength : int64 =
    let rec columnValues (values: int64 list) width num =
        if num = 0 then []
        else values.Head :: (columnValues (values.[(width)..]) width (num - 1))

    if not (paddingDown grid width adjacentLength) then 0L
    else product (columnValues grid width adjacentLength)

let calculateRight (grid: int64 list) width adjacentLength : int64 =
    if not (paddingRight grid width adjacentLength) then 0L
    else product grid.[..(adjacentLength - 1)]

let calculateDiagonalRight (grid: int64 list) width adjacentLength : int64 =
    let rec diagonalValuesRight (values: int64 list) width num =
        if num = 0 then []
        else values.Head :: (diagonalValuesRight values.[(width + 1)..] width (num - 1))

    if not (paddingDown grid width adjacentLength) || not (paddingRight grid width adjacentLength) then 0L
    else product (diagonalValuesRight grid width adjacentLength)

let calculateDiagonalLeft (grid: int64 list) width adjacentLength : int64 =
    let rec diagonalValuesLeft (values: int64 list) width num =
        if num = 0 then []
        else values.Head :: (diagonalValuesLeft values.[(width - 1)..] width (num - 1))

    if not (paddingDown grid width adjacentLength) || not (paddingLeft grid width adjacentLength) then 0L
    else product (diagonalValuesLeft grid width adjacentLength)

let maxOf a b = if a > b then a else b

let maxProduct grid width adjacentLength : int64 =
    List.fold maxOf 0L [
        calculateDown grid width adjacentLength
        calculateRight grid width adjacentLength
        calculateDiagonalRight grid width adjacentLength
        calculateDiagonalLeft grid width adjacentLength ]

let rec largestProduct (grid: int64 list) width adjacentLength : int64 =
    if grid.IsEmpty then 0L
    else maxOf (maxProduct grid width adjacentLength) (largestProduct grid.Tail width adjacentLength)

let solution = largestProduct (values "input" linesToGrid) 20 4

printfn "%A" solution
