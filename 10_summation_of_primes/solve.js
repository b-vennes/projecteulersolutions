// note: this one takes a while to run

const cpsRecursion = (tail) => {
    while (typeof tail === 'function') tail = tail();

    return tail;
};

const primes = (max) => {
    const primesHelper = (current, values) => {
        if (current >= max) 
            return values;
        else if (!values.some(value => current % value === 0)) 
            return () => primesHelper(current + 1, [...values, current]);
        else 
            return () => primesHelper(current + 1, values);
    }

    return cpsRecursion(primesHelper(3, [2]));
}

const sum = (accumulator, value) => accumulator + value;

const primesSum = primes(2000000).reduce(sum, 0);

console.log(primesSum);
