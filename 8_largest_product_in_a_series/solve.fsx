let linesToInts lines =
    lines
    |> Seq.fold (fun state line -> state + line) ""
    |> Seq.toList
    |> List.map (fun c-> c |> sprintf "%c" |> int64)

let values input parser = 
    System.IO.File.ReadLines(input)
    |> parser

let rec product (values: int64 list) = 
            if (values.IsEmpty) then 1L
            else values.Head * (product values.Tail)

let rec maxAdjacent pointFunc numDigits (digits: int64 list) =
    let largerOf a b =
        if (a > b) then a
        else b

    match digits.Length with
    | l when l < numDigits -> 0L
    | _ -> largerOf (pointFunc digits.[0..(numDigits-1)]) (maxAdjacent pointFunc numDigits digits.Tail)

let inputNumbers = values "input" linesToInts

printfn "%A" (maxAdjacent product 4 inputNumbers)
printfn "%A" (maxAdjacent product 13 inputNumbers)
