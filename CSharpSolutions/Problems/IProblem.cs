namespace CSharpSolver.Problems
{
    public interface IProblem
    {
        int Solve();
    }
}