using System.Linq;

namespace CSharpSolver.Problems
{
    public class Problem4 : IProblem
    {
        public int Solve()
        {
            var largest = -1;

            for (int i = 100; i <= 999; i++)
            {
                for (int j = 100; j <= 999; j++)
                {
                    var value = i * j;
                    largest = value > largest && IsPalindrome(value) ? value : largest;
                }
            }

            return largest;        
        }

        private bool IsPalindrome(int number)
        {
            int reversedNumber = int.Parse(new string(number.ToString().Reverse().ToArray()));

            if (number == reversedNumber)
                return true;
            
            return false;

        }
    }
}