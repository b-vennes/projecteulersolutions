using System.Linq;

namespace CSharpSolver.Problems
{
    public class Problem1 : IProblem
    {
        public int Solve()
        {
            var sum = Enumerable.Range(0,1000)
                .Where(number => number % 3 == 0 || number % 5 == 0)
                .Sum();

            return sum;
        }
    }
}