using System.Collections.Generic;
using System.Linq;

namespace CSharpSolver.Problems
{
    public class Problem2 : IProblem
    {
        public int Solve()
        {
            var sum = FibGenerator(4_000_000)
                .Where(num => num % 2 == 0)
                .Sum();

            return sum;
        }

        private List<int> FibGenerator(int max)
        {
            var fibs = new List<int>() { 1, 2 };

            while(fibs[fibs.Count - 1] < max)
            {
                var next = fibs[fibs.Count - 2] + fibs[fibs.Count - 1];
                fibs.Add(next);
            }

            return fibs;
        }
    }
}