using System;
using System.Collections.Generic;
using System.Linq;

namespace CSharpSolver.Problems
{
    public class Problem3 : IProblem
    {
        public int Solve()
        {
            long number = 600851475143;

            var factorsList = new List<int>();

            Enumerable.Range(2, (int)Math.Ceiling(Math.Sqrt(number)))
                .ToList()
                .ForEach(x => {
                    while (number % x == 0)
                    {
                        factorsList.Add(x);
                        number = number / x;
                    }
                });

            return factorsList.Last();
        }
    }
}