﻿using System;
using CSharpSolver.Problems;

namespace CSharpSolver
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("No problem number given");
                return;
            }

            int problemNumber;

            int.TryParse(args[0], out problemNumber);

            RunProblem(problemNumber);
        }

        private static void RunProblem(int problemNumber)
        {
            IProblem problem = problemNumber switch
            {
                1 => new Problem1(),
                2 => new Problem2(),
                3 => new Problem3(),
                4 => new Problem4(),
                _ => null
            };

            if (problem == null)
            {
                Console.WriteLine("Invalid problem");
            }
            else
            {
                Console.WriteLine($"Solution: {problem.Solve()}");
            }
        }
    }
}
